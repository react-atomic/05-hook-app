import {renderHook, act} from "@testing-library/react-hooks";
import {useForm} from "../../hooks/useForm";

describe('Pruebas en useForm', () => {

  const initialForm = {
    name: 'Adriel',
    email: 'aaron.ariperto@gmail.com'
  }


  test('debe de regresar un formulario por defecto', () => {

    const {result} = renderHook(() => useForm(initialForm));
    const [value, handleInputChange, reset] = result.current;

    expect(value).toEqual(initialForm);
    expect(typeof handleInputChange).toEqual('function');
    expect(typeof reset).toEqual('function');

  })


  test('debe de cambiar el valor del formulario (cambiar name)', () => {

    const {result} = renderHook(() => useForm(initialForm));
    const [, handleInputChange] = result.current;

    act(() => {
      handleInputChange({
        target: {
          name: 'name',
          value: 'aaron'
        }
      })
    })

    const [value] = result.current;

    expect(value).toEqual({...initialForm, name: 'aaron'});

  });

  test('debe re-establecer el formuralio con RESET', () => {

    const {result} = renderHook(() => useForm(initialForm));
    const [, handleInputChange, reset] = result.current;

    act(() => {
      handleInputChange({
        target: {
          name: 'name',
          value: 'aaron'
        }
      });

      reset();
    })

    const [value] = result.current;

    expect(value).toEqual(initialForm);

  });


})
